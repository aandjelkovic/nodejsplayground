--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-07-27 23:42:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 180 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 180
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 16461)
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accounts (
    id integer NOT NULL,
    openingdate date,
    amount money,
    status integer,
    lud time with time zone,
    name text
);


ALTER TABLE accounts OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16425)
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE projects (
    id integer NOT NULL,
    name text,
    startdate date,
    enddate date,
    status integer,
    projectaccountid integer
);


ALTER TABLE projects OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16433)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name text
);


ALTER TABLE roles OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16494)
-- Name: transactions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE transactions (
    id integer NOT NULL,
    sourceaccountid integer,
    destinationaccountid integer,
    date date,
    amount money,
    performedby integer,
    type integer
);


ALTER TABLE transactions OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16409)
-- Name: user_id_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_sequence OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16417)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name text
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 16469)
-- Name: users_accounts_roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_accounts_roles (
    userid integer NOT NULL,
    roleid integer NOT NULL,
    accountid integer NOT NULL
);


ALTER TABLE users_accounts_roles OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 16441)
-- Name: users_projects_roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users_projects_roles (
    userid integer NOT NULL,
    projectid integer NOT NULL,
    roleid integer NOT NULL
);


ALTER TABLE users_projects_roles OWNER TO postgres;

--
-- TOC entry 2046 (class 0 OID 16461)
-- Dependencies: 177
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accounts (id, openingdate, amount, status, lud, name) FROM stdin;
\.


--
-- TOC entry 2043 (class 0 OID 16425)
-- Dependencies: 174
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY projects (id, name, startdate, enddate, status, projectaccountid) FROM stdin;
\.


--
-- TOC entry 2044 (class 0 OID 16433)
-- Dependencies: 175
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY roles (id, name) FROM stdin;
\.


--
-- TOC entry 2048 (class 0 OID 16494)
-- Dependencies: 179
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY transactions (id, sourceaccountid, destinationaccountid, date, amount, performedby, type) FROM stdin;
\.


--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 172
-- Name: user_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_sequence', 1, false);


--
-- TOC entry 2042 (class 0 OID 16417)
-- Dependencies: 173
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name) FROM stdin;
1	Ris
2	Aleks
3	Baba
7	prep boy
11	tetka
12	Nemac
\.


--
-- TOC entry 2047 (class 0 OID 16469)
-- Dependencies: 178
-- Data for Name: users_accounts_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users_accounts_roles (userid, roleid, accountid) FROM stdin;
\.


--
-- TOC entry 2045 (class 0 OID 16441)
-- Dependencies: 176
-- Data for Name: users_projects_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users_projects_roles (userid, projectid, roleid) FROM stdin;
\.


--
-- TOC entry 1918 (class 2606 OID 16468)
-- Name: ix_account_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT ix_account_id PRIMARY KEY (id);


--
-- TOC entry 1912 (class 2606 OID 16432)
-- Name: ix_project_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT ix_project_id PRIMARY KEY (id);


--
-- TOC entry 1916 (class 2606 OID 16445)
-- Name: ix_projects_roles_users; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_projects_roles
    ADD CONSTRAINT ix_projects_roles_users PRIMARY KEY (userid, projectid, roleid);


--
-- TOC entry 1914 (class 2606 OID 16440)
-- Name: ix_role_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT ix_role_id PRIMARY KEY (id);


--
-- TOC entry 1922 (class 2606 OID 16498)
-- Name: ix_transaction; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT ix_transaction PRIMARY KEY (id);


--
-- TOC entry 1920 (class 2606 OID 16473)
-- Name: ix_users_accounts_roles; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_accounts_roles
    ADD CONSTRAINT ix_users_accounts_roles PRIMARY KEY (userid, roleid, accountid);


--
-- TOC entry 1910 (class 2606 OID 16424)
-- Name: pk_user_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT pk_user_id PRIMARY KEY (id);


--
-- TOC entry 1923 (class 2606 OID 16446)
-- Name: fk_projects_roles_users_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_projects_roles
    ADD CONSTRAINT fk_projects_roles_users_projects FOREIGN KEY (projectid) REFERENCES projects(id);


--
-- TOC entry 1924 (class 2606 OID 16451)
-- Name: fk_projects_roles_users_roles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_projects_roles
    ADD CONSTRAINT fk_projects_roles_users_roles FOREIGN KEY (roleid) REFERENCES roles(id);


--
-- TOC entry 1925 (class 2606 OID 16456)
-- Name: fk_projects_roles_users_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_projects_roles
    ADD CONSTRAINT fk_projects_roles_users_users FOREIGN KEY (userid) REFERENCES users(id);


--
-- TOC entry 1930 (class 2606 OID 16504)
-- Name: fk_transactions_accounts_destinationaccountid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_transactions_accounts_destinationaccountid FOREIGN KEY (destinationaccountid) REFERENCES accounts(id);


--
-- TOC entry 1931 (class 2606 OID 16509)
-- Name: fk_transactions_accounts_performedby; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_transactions_accounts_performedby FOREIGN KEY (performedby) REFERENCES users(id);


--
-- TOC entry 1929 (class 2606 OID 16499)
-- Name: fk_transactions_accounts_sourcaccountid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY transactions
    ADD CONSTRAINT fk_transactions_accounts_sourcaccountid FOREIGN KEY (sourceaccountid) REFERENCES accounts(id);


--
-- TOC entry 1927 (class 2606 OID 16479)
-- Name: fk_users_accounts_roles_accounts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_accounts_roles
    ADD CONSTRAINT fk_users_accounts_roles_accounts FOREIGN KEY (accountid) REFERENCES accounts(id);


--
-- TOC entry 1928 (class 2606 OID 16484)
-- Name: fk_users_accounts_roles_roles; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_accounts_roles
    ADD CONSTRAINT fk_users_accounts_roles_roles FOREIGN KEY (roleid) REFERENCES roles(id);


--
-- TOC entry 1926 (class 2606 OID 16474)
-- Name: fk_users_accounts_roles_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users_accounts_roles
    ADD CONSTRAINT fk_users_accounts_roles_users FOREIGN KEY (userid) REFERENCES users(id);


--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-07-27 23:42:24

--
-- PostgreSQL database dump complete
--

