var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var timeout = require('connect-timeout');
var velLaza = "Mamina maza";
// process.env.VASHPATH
var vash = require('vash');//, vruntime = require( process.env.VASHRUNTIMEPATH );

vash.config.useWith = true;
vash.config.debug = false;

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var passport = require('passport'),
    passportLocal = require('passport-local'),
	passportHttp = require('passport-http'),
    TwitterStrategy = require('passport-twitter'),
    GoolgeStrategy = require('passport-google'),
    FacebookStrategy = require('passport-facebook');
	
var session = require('express-session');
/*
var RedisStore = require('connect-redis')(session);
var options = {
	host: 'localhost',
	port: 6379,
	pass: 'xyzxyz2',
	db: 0,
	ttl: 600,
	prefix: 'gerosim:'
};
*/
	
var config = require('./config.js'), //config file contains all tokens and other private info
    funct = require('./functions.js');

var controllers = require('./controllers/index');

console.log("Hurray!"); //Way to debug express node js

var app = express(); 



app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
// view engine setup
app.set('view engine', 'vash');
app.set('views', __dirname + '/views');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
/*
app.use(session({
					store: new RedisStore(options),
					secret: 'gorgonzola speed',
					saveUninitialized: false,
					resave: false
				}));
*/
/* passport init() after session */
app.use(passport.initialize());
app.use(passport.session());

app.use('/', controllers);

app.use(function(req, res, next){
  var err = req.session.error,
      msg = req.session.notice,
      success = req.session.success;

  delete req.session.error;
  delete req.session.success;
  delete req.session.notice;

  if (err) res.locals.error = err;
  if (msg) res.locals.notice = msg;
  if (success) res.locals.success = success;

  next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.use(timeout(4000));
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next){
  console.log("WTF this is bad");
  if (!req.timedout) 
		next();
}

module.exports = app;







