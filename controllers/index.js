var express = require('express');
var router = express.Router();

var userdb = require('./dal/userdb.js'); 
/* TEST Block >>> */

var nextFn = function(req, res, next) {
	console.log("next");
	next();
};



/* Simple Response Test  >>> */
router.get('/ok1', nextFn, function(req, res) {
	res.send("OK");
});

router.get('/ok2/:id', function(req, res) {
	//res.status(200).send(req.params.id);
	res.status(200).send({id:1, name:"Ristevski"});
});

router.get('/OK3/:id', function(req, res) {
	res.send(req.params.id);
});

router.get('/T200', function(req, res) {
	res.status(200).end();
});

router.get('/T400', function(req, res) {
	res.status(400).end();
});

router.get('/T404', function(req, res) {
	res.status(404).end();
});

/* End Simple Response Test <<< */


/* Simple Page >>> */

//Index Page 
router.get('/', function(req, res) {
  res.render('index', { title: 'Simple Argument Pass' });
});


router.get('/index2', function(req, res) {
res.render('index_layout', { title: 'Opet Neka Poruka', messages: [{text:"Ovo je prva1"}, {text:"Ovo je 2"}, {text: "Ovo je 3"},{text: "Ovo je 4"} ] });
});


router.get('/index3', function(req, res) {
	userdb.selectUsers(
			function (dataGet) {
				res.render('index3', {result: dataGet});	
			}
  	);
  });

router.get('/users/edit', function(req, res) {
		//res.status(200).end();
		res.render('users/edit');	
	}
);

router.post('/users/edit', function(req, res) {
	console.log("Id:" + req.body.id);
	console.log("Name:" + req.body.name);
	var params = [req.body.id, req.body.name];
	userdb.insertUser(params, function (dataGet) {	 
		res.redirect('/index3'); 
	});
});

 
  
  
/* Simple Page <<< */

module.exports = router;


