module.exports = {
    strClean: function (ob) {
        return (typeof ob !== 'undefined') ? ob : "";
    },
    objClean: function (ob) {
        return (typeof ob !== 'undefined') ? ob: null;
    }
};