var pg = require('pg');

module.exports = {
  ConnectionString : "postgres://postgres:1234567@192.168.0.5/facebook2",
  selectQuery: function(query, fnOk) {
    var qu = query;
    var conn = this.ConnectionString;
    pg.connect(conn, function(err, client, done) {
      if(err) {
        console.error('@: Could not connect to postgres', err);
        fnOk({Errors:err});
      } else {
        client.query(qu, function(err, result) {
                          if(err) {
                            console.error('@: Error running query', err);
                            fnOk({Errors:err});
                          } else {
                            fnOk({Result: result.rows});
                          }
                     });
      }
      done();
    });
  },
};