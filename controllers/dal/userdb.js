var pg = require('pg');
var pgCN = require('./dal.js');


module.exports = { 
	selectUser: function(pg, conString, fnOk) {
		pg.connect(conString, function(err, client, done) {
			if(err) {
				//res.render('list', { title: 'Error' });
				console.error('@: Could not connect to postgres', err);
			} else {
				client.query('SELECT "id", "name" from user ', function(err, result) {
					if(err) {
						//res.render('list', { title: 'Error' });
						console.error('@: Error running query', err);
					} else {
						var resArray = [];
						result.rows.every(function(obj, ind, arr) {
							resArray.push({id:obj.id, name:obj.name});
							return true;
						});
						fnOk(resArray);
					}
					done();
				});
			} 
		});
	},
	
	
	
	
	
  selectWords: function(pg, conString, fnOk) {
    pg.connect(conString, function(err, client, done) {
      if(err) {
        console.error('@: Could not connect to postgres', err);
      } else {
        client.query('select w.id, w.word, wf.frequency from words1 w inner join words_frequency1 wf on wf.word_id = w.id inner join words_files wfi on wfi.word_id = w.id where wf.frequency < 500 and wfi.path_exist = 1 order by frequency limit 500;', 
          function(err, result) {
            if(err) {
              console.error('@: Error running query', err);
            } else {
              fnOk(result.rows);
            }
          }
         );
      }
      done();
    });
  },
  selectQuery: function(query, fnOk) {
    var qu = query;
    var conn = pgCN.ConnectionString;
    pg.connect(conn, function(err, client, done) {
      if (err) {
        console.error('@: Could not connect to postgres', err);
      } else {
        client.query(qu, function(err, result) {
                          if(err) {
                            console.error('@: Error running query', err);
                          } else {
                            fnOk(result.rows);
                          }
                     });
      }
      done();
    });
  },
  execQuery: function(query, params, fnOk) {
    var qu = query;
	var prm = params;
    var conn = pgCN.ConnectionString;
    pg.connect(conn, function(err, client, done) {
      if (err) {
        console.error('@: Could not connect to postgres', err);
      } else {
        client.query(qu, prm, function(err, result) {
                          if(err) {
                            console.error('@: Error running query', err);
                          } else {
                            fnOk(result.rows);
                          }
                     });
      }
      done();
    });
  },
  selectWord1: function(fnOk) {
    //var t = proradi;
    var query = ' select w.id, w.word, wf.frequency, wfi.url from words1 w \
                  inner join words_frequency1 wf on wf.word_id = w.id \
                  inner join words_files wfi on wfi.word_id = w.id \
                  where wf.frequency < 500 and wfi.path_exist = 1 order by frequency limit 500; ';
    this.selectQuery(query, fnOk);
  },
  selectUsers: function(fnOk) {
    var query = 'SELECT "id", "name" from users';
    this.selectQuery(query, fnOk);
  },
  insertUser: function(params, fnOk) {
    var query = 'INSERT INTO users ("id","name") values ($1, $2) RETURNING id';
    this.execQuery(query, params, fnOk);
  }
};