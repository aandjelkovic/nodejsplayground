var pgDAL = require('./dal.js');

module.exports = { 
  selectWords: function(fnOk) {
    var query = 'select w.id, w.word, wf.frequency, wfi.url from words1 w \
                 inner join words_frequency1 wf on wf.word_id = w.id \
                 inner join words_files wfi on wfi.word_id = w.id \
                 where wf.frequency < 500 and wfi.path_exist = 1 order by frequency limit 500;';
    pgDAL.selectQuery(query, fnOk);
  },
};